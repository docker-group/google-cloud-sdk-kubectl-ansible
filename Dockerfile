FROM gcr.io/google.com/cloudsdktool/cloud-sdk:308.0.0-alpine
RUN gcloud components install kubectl
RUN apk update && apk add ansible=~2.9
RUN echo "===> Installing ansible collections for convenience..."        && \
    ansible-galaxy collection install community.general && \
    ansible-galaxy collection install community.docker && \
    ansible-galaxy collection install community.kubernetes && \
    echo "===> Installing pip dependencies..."        && \
    pip3 install openshift
